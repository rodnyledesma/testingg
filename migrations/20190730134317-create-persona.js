'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('persona', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      cedula: {
        type: Sequelize.STRING
      },
      apellidos: {
        type: Sequelize.STRING
      },
      nombres: {
        type: Sequelize.STRING
      },
      fecha_nac: {
        type: Sequelize.DATEONLY
      },
      external_id: {
        type: Sequelize.UUID
      },
      nro_reg: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('persona');
  }
};