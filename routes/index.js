var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Medicos' });
});
router.get('/registro', function(req, res, next) {
  res.render('index', { title: 'Registro de Medicos', fragmento: 'fragmentos/form1', sesion:true });
});

router.get('/tablas', function(req, res, next) {
  res.render('index', { title: 'Pacientes', fragmento: 'fragmentos/tablas', sesion:true });
});

module.exports = router;
