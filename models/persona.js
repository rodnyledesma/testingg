'use strict';
module.exports = (sequelize, DataTypes) => {
  const persona = sequelize.define('persona', {
    cedula: DataTypes.STRING,
    apellidos: DataTypes.STRING,
    nombres: DataTypes.STRING,
    fecha_nac: DataTypes.DATEONLY,
    external_id: DataTypes.UUID,
    nro_reg: DataTypes.STRING
  }, {freezeTableName: true});
  persona.associate = function(models) {
    // associations can be defined here
    persona.hasOne(models.cuenta,{foreignKey:'id_persona', as: 'cuenta'});
  };
  return persona;
};